# You can use script "local_dash_env.py" to deploy local environment on your Ubuntu. Below you can see the instruction how to use it.

1. You should download script to your laptop. Please specify bitbucket user name in the link below.


```
git clone https://<user_name>@bitbucket.org/mkondratyk/devops.git

```
and give permission to run that script:


```
#!bash
chmod +x local_dash_env.py

```


You can use few option to run it:

- If you want to create local env from scratch. It should install all necessary tools like docker, docker-compose, git, pip etc.. and run containers.


```
#!bash

sudo ./local_dash_env.py --action create_env --bitbucket_username your_bitbucker_user --linux_username your_linux_user
```

For example 


```
#!bash
sudo ./local_dash_env.py --action create_env --bitbucket_username mkondratyk --linux_username nick
```

- If you want to rebuild your containers. It should stop, delete and start fresh containers.


```
#!bash
sudo ./local_dash_env.py --action rebuild_dash --bitbucket_username your_bitbucker_user --linux_username your_linux_user

```

- If you want to delete your containers at all. It should stop and delete all your containers.


```
#!bash

sudo ./local_dash_env.py --action destroy_env --bitbucket_username your_bitbucker_user --linux_username your_linux_user
```

- If you want to rebuild your containers and download the latest version of repo. Pay attention that this action will delete old local repo.

```
#!bash

sudo ./local_dash_env.py --action rebuild_env_with_repo --bitbucket_username your_bitbucker_user --linux_username your_linux_user
```